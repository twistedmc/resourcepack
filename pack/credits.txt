A few textures in this pack have come from a pack titled "ItemBound"
Author and creator of that pack is "SixFootBlue"

He has very kindly given me permission to update his old 1.12 textures to 1.13 to use on
the Twisted Vanilla.  All credit goes to him.  I have only updated the files so they
work with 1.13+.  I have made tweaks and added a few things of my own, but I wanted to 
make sure SixFootBlue gets credit as well.


LINKS
YouTube Link: https://www.youtube.com/channel/UCDcich_SsnItj5C1nDqu7fA/featured
Pack Link:    https://minecraft.curseforge.com/projects/itembound-16x?page=2
User Link:    https://www.planetminecraft.com/member/sixfootblue/
------------------------------------------------------------------------------------
A few textures in this pack have also come from a pack titled "Ghoulcraft"
Author and creator is "MissGhouls" and "Mizuno" 

I have been given permission to pull some of the items out of the pack and use them on
Twisted Vanilla. These are amazing so make sure you follow the links to see everything
they have created!

LINKS
Pack Link:    https://www.planetminecraft.com/texture_pack/ghoulcraft-cit-1-13-2-beta-v-1-0-mizunos-add-on/
User Link:    https://www.planetminecraft.com/member/missghouls/
User Link:    https://www.planetminecraft.com/member/mizuno/
Discord Link: https://discord.gg/xyz65DJ
------------------------------------------------------------------------------------
Staves Textures are made from nongfu (User link: https://www.planetminecraft.com/member/nongfu/)
Pack Link: https://www.planetminecraft.com/texture_pack/transform-bows-to-staves-mystic-staves-addon-12-unique-3d-models/
------------------------------------------------------------------------------------
Halloween Mobs are from the Texture Pack created by stapleshotz (Link to user: http://www.planetminecraft.com/member/stapleshotz/ )
Link to resource pack: http://www.planetminecraft.com/texture_pack/pumpkin-mobs/